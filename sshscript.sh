#!/bin/bash

#------------------------------------------------------------------#
# Auteur : Victor LOT-DEVOT
# Version : 1.0
# Licence : MIT
# Utilisation : Gérer ses connexions SSH en ligne de commande.
#------------------------------------------------------------------#

fichierServer="$HOME/.config/server.sqlite"
if [ -e $fichierServer ]; then
    # Définition des fonctions
    aideModifier() {
        echo -e "Syntaxe : ./sshscript -m <id machine> <paramètre>"
        echo -e "Paramètres : \n"
        echo -e "nom : Modifier le nom de la machine"
        echo -e "ip : Modifier l'IP de la machine"
        echo -e "groupe : Modifier le groupe de la machine"
        echo -e "user : Modifier l\'utilisateur pour la connexion"
        echo -e "cle : Modifier le chemin de la clé privée"
    }
    #Afficher la page d'aide
    affichageAide() {
        echo -e "\nCe script est un outil pour gérer ses profils SSH (comme Putty ou Remmina) mais en ligne de commande uniquement."
        echo -e "Syntaxe : ./scriptssh option\n"
        echo -e "Options :"
        #echo -e "-v | --version : Affiche la version"
        echo -e "-h | --help : Affiche cette page d'aide"
        echo -e "-l | --list : Liste toutes les machines enregistrées"
        echo -e "-c | --create : Ajoute une nouvelle machine"
        echo -e "-r | --remove : Supprime une machine enregistrée - Syntaxe : ./sshscript -r <id machine>"
        echo -e "-s | --start : Lancer une connexion - Syntaxe : ./sshscript -s <id machine>"
        echo -e "-m | --modify : Modifier une connexion - Syntaxe : ./sshscript -m <id machine>"
        echo -e "--reset : Réinitialiser la base de donnée. ATTENTION : TOUTES LES ENTRÉES SERONT EFFACÉES ! \n"
    }
    modifierMachine() {
        read -p "Saisir une adresse IP de la machine distante (laisser vide si pas de changement) : " ip
        if [ -z "$ip" ]; then
            echo "Pas de changement d'adresse IP"
        else
            ipValide=$(testIp $ip)
            while [ "$ipValide" != "OK" ]; do
                read -p "IP invalide ! Veuillez réésayer : " ip
                ipValide=$(testIp $ip)
            done
            sqlite3 -batch $fichierServer "UPDATE serveur SET ip = '$ip' WHERE rowid = "'"'$1'"'";"
        fi

        read -p "Saisir un nom commun de la machine distante (laisser vide si pas de changement) : " nom
        if [ -z "$nom" ]; then
            echo "Pas de changement de nom"
        else
            sqlite3 -batch $fichierServer "UPDATE serveur SET nom = '$nomMaj' WHERE rowid = "'"'$1'"'";"
        fi

        read -p "Saisir le port SSH de la machine distante (laisser vide si pas de changement) : " port
        if [ -z "$port" ]; then
            echo "Pas de changement de port"
        else
            portValide=$(testPort $port)
            while [ "$portValide" != "OK" ]; do
                read -p "Port incorrect ! Il doit être compris entre 20 et 65534 : " port
                portValide=$(testPort $port)
            done
            sqlite3 -batch $fichierServer "UPDATE serveur SET port = '$port' WHERE rowid = "'"'$1'"'";"
        fi
        read -p "Saisir le nom du groupe (laisser vide si pas de changement) : " groupe
        if [ -z "$groupe" ]; then
            echo "Pas de changement de groupe"
        else
            sqlite3 -batch $fichierServer "UPDATE serveur SET groupe = '$groupeMaj' WHERE rowid = "'"'$1'"'";"
        fi
        read -p "Saisir l'utilisateur par défaut pour les connexions (laisser vide si pas de changement) : " user
        if [ -z "$user" ]; then
            echo " Pas de changement de l'utilisateur de connexion"
        else
            sqlite3 -batch $fichierServer "UPDATE serveur SET user = '$user' WHERE rowid = "'"'$1'"'";"
        fi
        read -p "Saisir le chemin vers la clé privée (Optionnel) (laisser vide si pas de changement) : " cle
        if [ -z "$cle" ]; then
            echo "Pas de changement de clé privée"
        else
            sqlite3 -batch $fichierServer "UPDATE serveur SET cle = '$cle' WHERE rowid = "'"'$1'"'";"
        fi
    }

    # Enlever une machine de la base de données
    enleverMachine() {
        if [ -z $1 ]; then
            echo "Veuillez préciser un argument."
            echo "Syntaxe : ./sshscript -r <id machine>"
        else
            sqlite3 -batch $fichierServer "DELETE from serveur where rowid="'"'$1'"'";"
            sqlite3 -batch $fichierServer "VACUUM;"
            listeMachine
        fi
    }

    # Lister les serveurs enregistrés
    listeMachine() {
        if [ -z $1 ]; then
            sqlite3 -batch -header -column $fichierServer "SELECT rowid AS Identifiant,nom AS Machine,ip AS IP,port AS Port,groupe AS Groupe,user AS Utilisateur,cle AS Clé FROM serveur;"
            echo -e "\n"
        else
            case $1 in
            "--trier-par")
                case $2 in
                "nom")
                    sqlite3 -batch -header -column $fichierServer "SELECT rowid AS Identifiant,nom AS Machine,ip AS IP,port AS Port,groupe AS Groupe,user AS Utilisateur,cle AS Clé FROM serveur ORDER BY nom ASC;"
                    exit 0
                    ;;

                "groupe")
                    sqlite3 -batch -header -column $fichierServer "SELECT rowid AS Identifiant,nom AS Machine,ip AS IP,port AS Port,groupe AS Groupe,user AS Utilisateur,cle AS Clé FROM serveur WHERE groupe != "'"'""'"'" ORDER BY groupe ASC;"
                    exit 0
                    ;;

                *)
                    echo -e "Option non reconnue. Les options de tri disponibles sont : "
                    echo -e "nom : Trier par nom"
                    echo -e "groupe : Trier par groupe"
                    exit 0
                    ;;

                esac
                ;;

            "--uniq")
                case $2 in
                "nom")
                    nomUniq=$3
                    nomMajUniq=${nomUniq^^}
                    sqlite3 -batch -header -column $fichierServer "SELECT rowid AS Identifiant,nom AS Machine,ip AS IP,port AS Port,groupe AS Groupe,user AS Utilisateur,cle AS Clé FROM serveur WHERE nom = "'"'$nomMajUniq'"'";"
                    exit 0
                    ;;
                "groupe")
                    groupeUniq=$3
                    groupeMajUniq=${groupeUniq^^}
                    sqlite3 -batch -header -column $fichierServer "SELECT rowid AS Identifiant,nom AS Machine,ip AS IP,port AS Port,groupe AS Groupe,user AS Utilisateur,cle AS Clé FROM serveur WHERE groupe = "'"'$groupeMajUniq'"'";"
                    ;;
                *)
                    echo "Filtre non reconnu. Les filtres disponibles sont : "
                    echo "--trier-par <nom option tri> : trier par ordre croissant sur une colonne précisée."
                    echo "--uniq <nom|groupe> <nomMachine|nomGroupe> : Rechercher les entrées en fonction de leur nom ou leur groupe"
                    ;;
                esac
                ;;
            esac
        fi
    }

    # Se connecter aux machines enregistrées
    connexionMachine() {
        if [ -z $1 ]; then
            echo "Syntaxe : ./sshscript -s <id machine>"
            affichageAide
        else
            #nomCo=`sqlite3 -batch "SELECT nom FROM serveur WHERE rowid="'"'$1'"'";"`
            ipCo=$(sqlite3 -batch $fichierServer "SELECT ip FROM serveur WHERE rowid="$1";")
            portCo=$(sqlite3 -batch $fichierServer "SELECT port FROM serveur WHERE rowid="$1";")
            cleCo=$(sqlite3 -batch $fichierServer "SELECT cle FROM serveur WHERE rowid="$1";")
            userCo=$(sqlite3 -batch $fichierServer "SELECT user FROM serveur WHERE rowid="$1";")
            if [ -z $cleCo ]; then
                ssh -p "$portCo" "$userCo"@"$ipCo"
                exit 0
            else
                if [ -e $cleCo ]; then
                    ssh -p $portCo "$userCo"@"$ipCo" -i $cleCo
                else
                    echo "Le fichier est introuvable. Effacez le profil correspondant à cette machine, puis  reconfigurez la."
                    exit 0
                fi
            fi
        fi
    }

    resetBase() {
        sqlite3 -batch $fichierServer "DELETE from serveur;"
        listeMachine
    }

    #Ajouter une machine dans la base de données
    creationMachine() {
        read -p "Saisir une adresse IP de la machine distante : " ip
        ipValide=$(testIp $ip)
        while [ "$ipValide" != "OK" ]; do
            read -p "Adresse IP incorrecte, réésayez : " ip
            ipValide=$(testIp $ip)
        done
        read -p "Saisir un nom commun de la machine distante : " nom
        while [ -z "$nom" ]; do
            read -p "Veuillez saisir un nom ! " nom
        done
        read -p "Saisir le port SSH de la machine distante : " port
        portValide=$(testPort $port)
        while [ "$portValide" != "OK" ]; do
            read -p "Port incorrect ! Il doit être compris entre 20 et 65534 : " port
            portValide=$(testPort $port)
        done
        read -p "Saisir le nom du groupe (optionnel) : " groupe
        read -p "Saisir l'utilisateur par défaut pour les connexions : " user
        read -p "Saisir le chemin vers la clé privée (optionnel) : " cle
        nomMaj=${nom^^}
        groupeMaj=${groupe^^}

        siExisteNom=$(sqlite3 -batch $fichierServer "SELECT nom FROM serveur where nom="'"'$nomMaj'"'";")
        if [ "$siExisteNom" != "$nomMaj" ]; then
            siExisteIp=$(sqlite3 -batch $fichierServer "SELECT ip FROM serveur WHERE ip="'"'$ip'"'";")
            if [ "$siExisteIp" != "$ip" ]; then
                sqlite3 -batch $fichierServer "INSERT INTO serveur (nom,ip,port,groupe,user,cle) VALUES('$nomMaj','$ip','$port','$groupeMaj','$user','$cle');"
                listeMachine
            else
                echo -e "\nUne entrée avec cette adresse IP existe déjà !\n"
                listeMachine
            fi
        else
            echo -e "\n\nUne entrée avec ce nom existe déjà ! Veuillez recommencer et choisir un autre nom."
            listeMachine

        fi
    }
    testPort() {
        saisiePort=$1
        if [ $saisiePort -lt 20 ] || [ $saisiePort -ge 65535 ]; then
            echo "Port invalide ! Il doit être compris entre 20 et 65534."
        else
            local validePort='OK'
            echo "$validePort"
        fi
    }

    # Test du format des adresses IP
    function testIp() {
        valide=0
        saisieIp=$1
        for IP_ADDRESS in $saisieIp; do
            #this var is a test that will see if the number is beetween 1 and 254
            test='([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])'
            if [[ $IP_ADDRESS =~ ^$test\.$test\.$test\.$test$ ]]; then
                #echo $IP_ADDRESS
                local valide='OK'
                echo "$valide"
            else
                echo -e "L'adresse IP est invalide : $IP_ADDRESS"
            fi
        done
    }

    # Test de la saisie utilisateur
    if [ -z $1 ]; then
        affichageAide
    else
        case $1 in
        "-h" | "--help")
            affichageAide
            ;;
        "-l" | "--list")
            listeMachine $2 $3 $4
            ;;
        "-c" | "--create")
            creationMachine
            ;;
        "-s" | "--start")
            if [ -z $2 ]; then
                echo -e "Aucune machine sélectionnée, veuillez en choisir une : \n"
                affichageAide
                listeMachine
            else
                connexionMachine $2
            fi
            ;;
        "-r" | "--remove")
            if [ -z $2 ]; then
                echo -e "Aucune machine sélectionnée, veuillez en choisir une : \n"
                listeMachine
            else
                enleverMachine $2
            fi
            ;;
        "-m" | "--modify")
            if [ -z $2 ]; then
                echo -e "Aucune machine sélectionnée, veuillez en choisir une : \n"
                listeMachine
            else
                modifierMachine $2

            fi
            ;;
        "--reset")
            read -p "Voulez-vous vraiment effacer vos entrées ? <y/n> : " choix
            if [ "$choix" = "y" ]; then
                resetBase
            else
                echo "Annulé."
                exit 0
            fi
            ;;

        *)
            echo -e "Option inconnue, veuillez réésayer.\n"
            affichageAide
            ;;
        esac
    fi
else
    echo -e "Fichier de base de données inexistant... Création en cours... Veuillez relancer le script."
    touch $HOME/.config/server.sqlite
    sqlite3 -batch $fichierServer "CREATE TABLE serveur (nom varchar(50) NOT NULL, ip varchar(15) NOT NULL, port varchar(5) NOT NULL, groupe varchar(50), user varchar(50) NOT NULL );"
    echo -e "Le fichier a bien été créé dans $HOME/.config/server.sqlite"
fi
